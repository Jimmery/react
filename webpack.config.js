//进行webpack打包配置
const path = require("path");

module.exports = {
    mode:'production',
    entry: path.resolve(__dirname, "./src/routes/route.js"),
    output: {
        path: __dirname + '/src/dist',
		filename: "bundle.js"
    },
    devtool: 'eval-source-map',  //生成source file
    module: {
        loaders: [
            {
              test: /\.js$/,
              exclude: /node_modules/,
              loader: 'babel',
              query: {
                presets: ['es2015', 'react']
              }
            }
          ],
    }
};