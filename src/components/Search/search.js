import React, { Component } from "react";
import Styles from "./search.less";

export default class SearchInput extends Component {
    constructor(props) {
        super(props);
    };
    render() {
        const { type, placeholderText } = this.props;
        return (
            <input type={type} placeholder={placeholderText} />
        );
    };
}